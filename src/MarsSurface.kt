class MarsSurface(val maxX: Int, val maxY: Int) {

    private val robots = mutableListOf<Robot>()
    private val scents = mutableListOf<Pair<Position, Char>>()

    fun addRobot(robot: Robot): Int {
        robot.isAlive = positionIsOnSurface(robot.position)
        robots.add(robot)
        return robots.lastIndex
    }

    fun processCommands(robotId: Int, commands: String) {
        val robot = robots[robotId]
        if (!robot.isAlive) return

        commands.forEach { command ->
            val currentPosition = robot.position.copy()
            if (scents.contains(Pair(currentPosition, command))) {
                return@forEach
            }

            robot.processCommand(command)
            if (!positionIsOnSurface(robot.position)) {
                robot.isAlive = false
                robot.position = currentPosition
                scents.add(Pair(currentPosition, command))
                return
            }
        }
    }

    fun getRobots() = robots.toList()

    private fun positionIsOnSurface(position: Position): Boolean {
        return position.x in 0..maxX && position.y in 0..maxY
    }
}