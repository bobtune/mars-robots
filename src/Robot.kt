class Robot(var position: Position) {

    private val directions = arrayOf('N', 'E', 'S', 'W')
    private var directionIndex = directions.indexOfFirst {
        it == position.direction
    }

    var isAlive = true

    fun processCommand(command: Char) {
        when (command) {
            'L' -> turnLeft()
            'R' -> turnRight()
            'F' -> move()
        }
    }

    fun getDirection() = position.direction

    fun getCoordinates() = Pair(position.x, position.y)

    fun reportPosition(): String {
        return when (isAlive) {
            true -> position.reportPosition()
            false -> "${position.reportPosition()} LOST"
        }
    }

    private fun turnRight() {
        directionIndex = (directionIndex + 1) % directions.size
        position.direction = directions[directionIndex]
    }

    private fun turnLeft() {
        directionIndex = (directionIndex + directions.size - 1) % directions.size
        position.direction = directions[directionIndex]
    }

    private fun move() {
        when (directionIndex) {
            0 -> position.y++
            1 -> position.x++
            2 -> position.y--
            3 -> position.x--
        }
    }
}