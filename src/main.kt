fun main(args: Array<String>) {
    val coords = inputSurfaceCoords()
    val surface = MarsSurface(coords.first, coords.second)

    while (true) {
        val robotPosition = inputRobotPosition() ?: break
        val robot = Robot(robotPosition)
        val robotId = surface.addRobot(robot)

        print("commands? ")
        val commands = readLine() ?: break
        surface.processCommands(robotId, commands)
    }

    val robots = surface.getRobots()
    robots.forEach {
        println(it.reportPosition())
    }
}

private fun inputSurfaceCoords(): Pair<Int, Int> {
    print("top right coords? ")
    val topRight = readLine()?.split(" ")
    val x = topRight?.get(0)?.toInt() ?: 5
    val y = topRight?.get(1)?.toInt() ?: 3

    return Pair(x, y)
}

private fun inputRobotPosition(): Position? {
    print("robot position? ")
    val position = readLine()?.split(" ")
    if (position == null || position.size < 3) {
        return null
    }

    val x = position[0].toInt()
    val y = position[1].toInt()
    val d = position[2].toCharArray()

    return Position(x, y, d[0])
}