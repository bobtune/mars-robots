data class Position(var x: Int, var y: Int, var direction: Char) {

    fun reportPosition() = "$x $y $direction"
}