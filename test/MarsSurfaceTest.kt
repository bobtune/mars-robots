import org.junit.Test

import org.junit.Assert.*
import org.junit.Before

class MarsSurfaceTest {

    private lateinit var surface: MarsSurface

    @Before
    fun setUp() {
        surface = MarsSurface(5, 3)
    }

    @Test
    fun `test when add robot then returns correct id`() {
        val robot = Robot(Position(0, 0, 'N'))
        val id0 = surface.addRobot(robot)
        val id1 = surface.addRobot(robot)

        assertEquals(0, id0)
        assertEquals(1, id1)
    }

    @Test
    fun `test when add robot with valid coordinates then is alive`() {
        val robot = Robot(Position(2, 3, 'N'))
        surface.addRobot(robot)

        assertTrue(robot.isAlive)
    }

    @Test
    fun `test when add robot with invalid x coordinate then is not alive`() {
        val robot = Robot(Position(6, 3, 'N'))
        surface.addRobot(robot)

        assertFalse(robot.isAlive)
    }

    @Test
    fun `test when add robot with invalid y coordinate then is not alive`() {
        val robot = Robot(Position(2, 4, 'N'))
        surface.addRobot(robot)

        assertFalse(robot.isAlive)
    }

    @Test
    fun `test sample commands 1`() {
        val robot0 = Robot(Position(1, 1, 'E'))
        var id = surface.addRobot(robot0)
        surface.processCommands(id, "RFRFRFRF")

        assertEquals(Position(1, 1, 'E'), robot0.position)
        assertTrue(robot0.isAlive)

        val robot1 = Robot(Position(3, 2, 'N'))
        id = surface.addRobot(robot1)
        surface.processCommands(id, "FRRFLLFFRRFLL")

        assertEquals(Position(3, 3, 'N'), robot1.position)
        assertFalse(robot1.isAlive)

        val robot2 = Robot(Position(0, 3, 'W'))
        id = surface.addRobot(robot2)
        surface.processCommands(id, "LLFFFLFLFL")

        assertEquals(Position(2, 3, 'S'), robot2.position)
        assertTrue(robot2.isAlive)
    }
}