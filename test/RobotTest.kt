import org.junit.Test
import kotlin.test.assertEquals

class RobotTest {

    @Test
    fun `test when turn right from north then direction is east`() {
        val robot = Robot(Position(0, 0, 'N'))
        robot.processCommand('R')
        assertEquals('E', robot.getDirection())
    }

    @Test
    fun `test when turn right from east then direction is south`() {
        val robot = Robot(Position(0, 0, 'E'))
        robot.processCommand('R')
        assertEquals('S', robot.getDirection())
    }

    @Test
    fun `test when turn right from south then direction is west`() {
        val robot = Robot(Position(0, 0, 'S'))
        robot.processCommand('R')
        assertEquals('W', robot.getDirection())
    }

    @Test
    fun `test when turn right from west then direction is north`() {
        val robot = Robot(Position(0, 0, 'W'))
        robot.processCommand('R')
        assertEquals('N', robot.getDirection())
    }

    @Test
    fun `test when turn left from north then direction is west`() {
        val robot = Robot(Position(0, 0, 'N'))
        robot.processCommand('L')
        assertEquals('W', robot.getDirection())
    }

    @Test
    fun `test when turn left from west then direction is south`() {
        val robot = Robot(Position(0, 0, 'W'))
        robot.processCommand('L')
        assertEquals('S', robot.getDirection())
    }

    @Test
    fun `test when turn left from south then direction is east`() {
        val robot = Robot(Position(0, 0, 'S'))
        robot.processCommand('L')
        assertEquals('E', robot.getDirection())
    }

    @Test
    fun `test when turn left from east then direction is north`() {
        val robot = Robot(Position(0, 0, 'E'))
        robot.processCommand('L')
        assertEquals('N', robot.getDirection())
    }

    @Test
    fun `test when move north then y increases`() {
        val robot = Robot(Position(2, 2, 'N'))
        robot.processCommand('F')
        assertEquals(Pair(2, 3), robot.getCoordinates())
    }

    @Test
    fun `test when move east then x increases`() {
        val robot = Robot(Position(2, 2, 'E'))
        robot.processCommand('F')
        assertEquals(Pair(3, 2), robot.getCoordinates())
    }

    @Test
    fun `test when move south then y decreases`() {
        val robot = Robot(Position(2, 2, 'S'))
        robot.processCommand('F')
        assertEquals(Pair(2, 1), robot.getCoordinates())
    }

    @Test
    fun `test when move west then x decreases`() {
        val robot = Robot(Position(2, 2, 'W'))
        robot.processCommand('F')
        assertEquals(Pair(1, 2), robot.getCoordinates())
    }
}