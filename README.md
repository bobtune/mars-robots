# mars-robots
Mars robots coding problem in kotlin

I spent nearly three hours on the test so have left it as it is. The data entry is very basic and with no error checking. There are a couple of things in the brief which I haven't implemented such as maximum command length and maximum surface size, but these would be fairly simple to add. I would also have liked to have added more robust error checking in the main classes.

The app is written in Kotlin using IntelliJ community edition and the unit tests are Junit4. You can build the app and run the unit tests from that ide, or from the command line. I have produced the jar for the app which can be run using the java command line (java -jar mars.jar).